## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?

Sobre leitura eu posso afirmar que mais de 90% do conteúdo que eu leio vem da internet. Tutoriais, api's, ebook's e blogs estão no topo da minha lista de leitura. Os dois últimos livros que li foram estes:
- A Biblia do PHP 5 e Mysql
- Certificação Lpi-1 (101 - 102) - Col. Linux Pro - 4 Ed. 2014


2. Quais foram os últimos dois framework javascript que você trabalhou?

Jquery e Angular JS, tive uma experiência com SailsJS trabalhando server side com NodeJs.

3. Descreva os principais pontos positivos de seu framework favorito.

Vou citar alguns dos pontos fortes da jQuery, são eles:
- É um framework muito popular, então fica fácil de se encontrar soluções para diversas situações
- Também por causa de sua popularidade, é imensurável a quantidade de plugins desenvolvidos baseados em jquery
- Facilita o desenvolvimento para diversos browsers com uma sitaxe única, agilizando o desenvolvimento de aplicações


4. Descreva os principais pontos negativos do seu framework favorito.

É difícil encontrar um ponto negativo na jQuery, mas para não deixar esta pergunta em branco vou usar a desculpa de prache dos desenvolvedores AngularJs que é 

5. O que é código de qualidade para você?


## Conhecimento de desenvolvimento

1. O que é GIT?

2. Descreva um workflow simples de trabalho utilizando GIT.

3. Como você avalia seu grau de conhecimento em Orientação a objeto?

4. O que é Dependency Injection?

5. O significa a sigla S.O.L.I.D?

6. O que é BDD e qual sua importância para o processo de desenvolvimento?

7. Fale sobre Design Patterns.

9. Discorra sobre práticas que você considera boas no desenvolvimento javascript.
